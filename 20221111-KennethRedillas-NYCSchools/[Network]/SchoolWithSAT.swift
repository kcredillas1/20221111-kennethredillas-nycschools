//
//  SchoolWithSAT.swift
//  20221111-KennethRedillas-NYCSchools
//
//  Created by Kenneth Redillas on 11/10/22.
//

import Foundation

struct SchoolWithSAT {
    let school: School
    let sat: SAT
    init(school: School, sat: SAT) {
        self.school = school
        self.sat = sat
    }
}

extension SchoolWithSAT: Identifiable, Hashable {
    var id: String { school.dbn }
    
    static func == (lhs: SchoolWithSAT, rhs: SchoolWithSAT) -> Bool {
        lhs.school.dbn == rhs.school.dbn
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
