//
//  _0221111_KennethRedillas_NYCSchoolsApp.swift
//  20221111-KennethRedillas-NYCSchools
//
//  Created by Kenneth Redillas on 11/10/22.
//

import SwiftUI

@main
struct _0221111_KennethRedillas_NYCSchoolsApp: App {
    
    @StateObject var viewModel = ViewModel()
    var body: some Scene {
        WindowGroup {
            NavigationStack(path: $viewModel.navigationPath) {
                SchoolListView(viewModel: viewModel)
            }
        }
    }
}
