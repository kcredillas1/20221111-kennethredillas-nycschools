//
//  _0221111_KennethRedillas_NYCSchoolsTests.swift
//  20221111-KennethRedillas-NYCSchoolsTests
//
//  Created by Kenneth Redillas on 11/10/22.
//

import XCTest
@testable import _0221111_KennethRedillas_NYCSchools

class _0221111_KennethRedillas_NYCSchoolsTests: XCTestCase {
    
    func testSchools() {
        let expectation = XCTestExpectation(description: "Download data expectation.")
        let viewModel = ViewModel()
        viewModel.fetchSchools()
        DispatchQueue.global(qos: .background).async {
            Thread.sleep(forTimeInterval: 10)
            if viewModel.schools.count > 0 {
                expectation.fulfill()
            } else {
                XCTFail("No schools found")
            }
        }
        wait(for: [expectation], timeout: 15.0)
    }
    
}
